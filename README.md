# melawy-core-repo
Linux repo of my arch linux packages

```bash
sudo pacman-key --recv-keys 95F48000540A4DB146583A47C49B5E77FD80302D --keyserver hkps://keys.openpgp.org
sudo pacman-key --lsign-key 95F48000540A4DB146583A47C49B5E77FD80302D

sudo pacman-key --recv-keys BC8B600E8DDA1F4CB77B10D2BA803A261A5EE6B8 --keyserver hkps://keys.openpgp.org
sudo pacman-key --lsign-key BC8B600E8DDA1F4CB77B10D2BA803A261A5EE6B8
```

или

```bash
sudo pacman-key --lsign-key 95F48000540A4DB146583A47C49B5E77FD80302D --keyserver hkps://keyserver.ubuntu.com
sudo pacman-key --lsign-key 95F48000540A4DB146583A47C49B5E77FD80302D

sudo pacman-key --lsign-key BC8B600E8DDA1F4CB77B10D2BA803A261A5EE6B8 --keyserver hkps://keyserver.ubuntu.com
sudo pacman-key --lsign-key BC8B600E8DDA1F4CB77B10D2BA803A261A5EE6B8
```

```bash
sudo pacman -S melawy-linux-mirrorlist
```

Добавить в /etc/pacman.conf

```
[melawy-core]
SigLevel = Required DatabaseOptional
Include = /etc/pacman.d/melawy-linux-mirrorlist
```

### Donate
[Tinkoff](https://www.tinkoff.ru/rm/fadeeva.valeriya96/9bLRi79066)

[YooMoney](https://yoomoney.ru/to/4100115921160758)

[Qiwi](https://qiwi.com/n/VALERIAFADEEVA)

Etherium 0x981FBf878fe451BDB83BEaF68078394d4B13213f
